package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class About extends AppCompatActivity {
    WebView browser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
         browser = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = browser.getSettings();
        webSettings.setJavaScriptEnabled(true);
        browser.loadUrl("https://inmortaltechnologies.com/about.html");
    }

    @Override
    public void onBackPressed() {
        if(browser.canGoBack()){
            onBackPressed();
        }else{

            super.onBackPressed();
        }
    }
}