package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class login extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    EditText pass_word, phone_no;
    Button btnmobileLogin;
    Boolean isLogin;
    Boolean cancel;
    SharedPreferences mSharedPreference;
    CountryCodePicker ccp;
    SweetAlertDialog sweetAlertDialog;
    static final String pref_name = "Scratch";
    Progress progress;

    NetworkCall networkCall;
    String myname, mymobile_number, mypassword, userid, token,wallletAmount,selected_country_code;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));
        token = (mSharedPreference.getString("token", ""));
        myname = (mSharedPreference.getString("name", ""));
        mymobile_number = (mSharedPreference.getString("mobile", ""));
      //  wallletAmount = (mSharedPreference.getString("walllet_amount", ""));
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        selected_country_code = ccp.getSelectedCountryCodeWithPlus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                selected_country_code = ccp.getSelectedCountryCodeWithPlus();
            }
        });
        isLogin = mSharedPreference.getBoolean("login", false);
        if (isLogin) {
            Intent intent = new Intent(login.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        progress = new Progress(login.this);
        networkCall = new NetworkCall(login.this, login.this);

        phone_no = (EditText) findViewById(R.id.phoneNumberLogin);
        pass_word = (EditText) findViewById(R.id.passwordLogin);

        btnmobileLogin = (Button) findViewById(R.id.login);
        btnmobileLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mymobile_number = phone_no.getText().toString().trim();
                mypassword = pass_word.getText().toString().trim();
                phone_no.setError(null);
                pass_word.setError(null);

                if (TextUtils.isEmpty(mymobile_number)) {
                    phone_no.setError("Phone number is Required");
                    cancel = true;
                }
                else if(!mymobile_number.matches(regexStr)) {
                    phone_no.setError("Please Enter Valid Phone no.");
                    cancel = true;
                }
                else if (TextUtils.isEmpty(mypassword)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                }

                else if (pass_word.length() < 6) {
                    pass_word.setError("Password minimum contain 6 character");
                    cancel = true;
                }

                else {

                    login();


                }
            }
        });


    }

    private void login() {
//        Ion.with(this)
//                .load("POST", ApiURL.user_login)
//                .setHeader("Authorization",ApiURL.Login_Token)
//                .setBodyParameter("mobile", mymobile_number)
//                .setBodyParameter("password", mypassword)
//                .asString()
//                .setCallback(new FutureCallback<String>() {
//                    @Override
//                    public void onCompleted(Exception e, String result) {
//                        try {
//                            JSONObject reader = new JSONObject(result);
//
//
//                        } catch (JSONException e1) {
//                            e1.printStackTrace();
//                        }
//                    }
//                });

        networkCall.NetworkAPICall(ApiURL.user_login, true);

    }


    public void signUp(View view) {
        Intent intent = new Intent(login.this, Registration.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_login:
                ion = (Builders.Any.B) Ion.with(login.this)
                        .load("POST", ApiURL.user_login)
                        .setHeader("token", "b27585828a675f5acfef052dd1a8cf0c6c1ee4b0")
                        .setBodyParameter("mobile", mymobile_number)
                        .setBodyParameter("password", mypassword);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_login:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");
                    String token = jsonObject.getString("token");

                    if (status.equals("true")) {

                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("id");
                            myname = Jasonobject1.getString("name");
                            mymobile_number = Jasonobject1.getString("mobile");
                            wallletAmount = Jasonobject1.getString("walllet_amount");
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("name", myname);
                        editor.putString("id", userid);
                        editor.putString("token", token);
                        editor.putString("mobile", mymobile_number);
                        editor.putString("walllet_amount", wallletAmount);
                        editor.commit();
                        mSharedPreference.edit().putBoolean("login", true).commit();


                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Loot 2021");
                        sweetAlertDialog.setContentText(msg);
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(login.this, R.color.black));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                Intent intent = new Intent(login.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Loot 2021")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Loot 2021")
                            .setContentText("Oops... Something went wrong!")
                            .show();
                    // Toast.makeText(login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(login.this, jsonstring, Toast.LENGTH_SHORT).show();

    }

    public void forgot(View view) {
        Intent intentforgot = new Intent(login.this,ForgotPassword.class);
        startActivity(intentforgot);
        login.this.finish();
    }
}