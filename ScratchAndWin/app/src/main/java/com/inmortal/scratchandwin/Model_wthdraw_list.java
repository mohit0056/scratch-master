package com.inmortal.scratchandwin;

import java.io.Serializable;

public class Model_wthdraw_list implements Serializable {

    private int id;

    private String user_id;

    private String walllet_amount;

    private String withdrawal_request;

    private String created_at;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setWalllet_amount(String walllet_amount) {
        this.walllet_amount = walllet_amount;
    }

    public String getWalllet_amount() {
        return this.walllet_amount;
    }

    public void setWithdrawal_request(String withdrawal_request) {
        this.withdrawal_request = withdrawal_request;
    }

    public String getWithdrawal_request() {
        return this.withdrawal_request;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String date) {
        this.created_at = created_at;
    }

}



