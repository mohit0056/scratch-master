package com.inmortal.scratchandwin;


public class ScratchModel {
    private int id;

    private String skretch_amount;

    private String created_at;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setSkretch_amount(String skretch_amount) {
        this.skretch_amount = skretch_amount;
    }

    public String getSkretch_amount() {
        return this.skretch_amount;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return this.created_at;
    }
}

