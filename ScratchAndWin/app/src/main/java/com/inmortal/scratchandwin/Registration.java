package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;


import org.json.JSONException;
import org.json.JSONObject;



public class Registration extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    EditText name, pass_word, phone_no, confirm_password;
    Button btnmobileRegister;
    CheckBox chkCondition;
    Boolean isLogin;
    Boolean cancel;
    SweetAlertDialog sweetAlertDialog;

    static final String pref_name = "Scratch";
    Progress progress;
    CountryCodePicker ccp;

    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    String myname, mymobile_number, mypassword, mconfpass, user_id, token,selected_country_code;
    ImageView hidebtn, confhidebtn;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));
        token = (mSharedPreference.getString("token", ""));

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        selected_country_code = ccp.getSelectedCountryCodeWithPlus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                selected_country_code = ccp.getSelectedCountryCodeWithPlus();
            }
        });
        progress = new Progress(Registration.this);
        networkCall = new NetworkCall(Registration.this, Registration.this);

        phone_no = (EditText) findViewById(R.id.phoneNumberRegistration);
        pass_word = (EditText) findViewById(R.id.passwordRegistration);
        name = (EditText) findViewById(R.id.nameRegister);
        confirm_password = (EditText) findViewById(R.id.confpasswordRegistration);
        btnmobileRegister = (Button) findViewById(R.id.register);
        btnmobileRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myname = name.getText().toString().trim();
                mymobile_number = phone_no.getText().toString().trim();
                mypassword = pass_word.getText().toString().trim();
                mconfpass = confirm_password.getText().toString().trim();

                name.setError(null);
                phone_no.setError(null);
                pass_word.setError(null);
                confirm_password.setError(null);


                if (TextUtils.isEmpty(myname)) {
                    name.setError("Your Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mymobile_number)) {
                    phone_no.setError("Phone number is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypassword)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                }
                else if(!mymobile_number.matches(regexStr)) {
                    phone_no.setError("Please Enter Valid Phone no.");
                    cancel = true;
                }
                else if (mypassword.length() < 6) {
                    pass_word.setError("Password minimum contain 6 character");
                    cancel = true;
                } else if (TextUtils.isEmpty(mconfpass)) {
                    confirm_password.setError("Re-enter Password is required");
                    cancel = true;
                } else if (!mconfpass.equals(mypassword)) {
                    confirm_password.setError("Both Passwords are Different");
                    cancel = true;
                } else {

                    signUpMobile();


                }
            }
        });

    }


    private void signUpMobile() {

        networkCall.NetworkAPICall(ApiURL.user_register, true);

    }

    public void signin(View view) {
        Intent intent = new Intent(Registration.this, login.class);
        startActivity(intent);
        this.finish();

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_register:
                ion = (Builders.Any.B) Ion.with(Registration.this)
                        .load("POST", ApiURL.user_register)
                        .setBodyParameter("fullname", myname)
                        .setBodyParameter("mobile", mymobile_number)
                        .setBodyParameter("password", mypassword)
                        .setBodyParameter("confirm_password", mconfpass);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.user_register:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Loot 2021");
                        sweetAlertDialog.setContentText(msg);
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(Registration.this, R.color.black));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                Intent intent = new Intent(Registration.this, login.class);
                                startActivity(intent);
                                finish();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Loot 2021")
                                    .setContentText(msg)
                                    .show();

//                            Toast.makeText(Registration.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {



                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Loot 2021")
                                .setContentText("Oops... Something went wrong!")
                                .show();

                    //Toast.makeText(Registration.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}