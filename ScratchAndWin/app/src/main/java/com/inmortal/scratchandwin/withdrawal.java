package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class withdrawal extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, withdraw_list_recy_adapter.ReturnView {
    SharedPreferences mSharedPreference;
    static final String pref_name = "Scratch";
    String walletAmount, mywithdrawAmount, userid;
    Boolean cancel;
    SwipeRefreshLayout mSwipeRefreshLayout;
    SweetAlertDialog sweetAlertDialog;
    Button withdrawbtn;
    EditText withdrawmoney,paytmPhone;
    private HashSet<String> hashSet = new HashSet<String>();
    String myname, mymobile_number, scratchedAmount, list_Scratch_id, status,mypaytmPhone;
    TextView name, phone, amount;
    RecyclerView recyclerView;
    ArrayList<Model_wthdraw_list> arrmodelwithdrawhistory = new ArrayList<>();
    withdraw_list_recy_adapter withdrawalHistory_recy_adapter;
    Model_wthdraw_list model_withdraw_history;
    Integer addmoney, newmyINR;
    Progress progress;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";

    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);

        progress = new Progress(withdrawal.this);
        networkCall = new NetworkCall(withdrawal.this, withdrawal.this);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        scratchedAmount = (mSharedPreference.getString("scratchAmount", ""));
        mymobile_number = (mSharedPreference.getString("mobile", ""));
        list_Scratch_id = (mSharedPreference.getString("list_Scratch_id", ""));
        myname = (mSharedPreference.getString("name", ""));
        userid = (mSharedPreference.getString("id", ""));
        status = (mSharedPreference.getString("withdrawal_request", ""));
        walletAmount = (mSharedPreference.getString("walllet_amount", ""));
        name = findViewById(R.id.username);
        phone = findViewById(R.id.phoneNumber);
        paytmPhone = findViewById(R.id.Paytmno);
        amount = findViewById(R.id.amount);
        name.setText(myname);
        phone.setText(mymobile_number);

        if (walletAmount.equals("null")) {
            amount.setText("0");
        } else {
            amount.setText(walletAmount);
        }

        withdrawmoney = findViewById(R.id.withdrawalAmount);
        recyclerView = findViewById(R.id.recycler_amount_withdraw);
        withdrawbtn = findViewById(R.id.withdrawBtn);
        withdrawbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mywithdrawAmount = withdrawmoney.getText().toString().trim();
                mypaytmPhone = paytmPhone.getText().toString().trim();
                withdrawmoney.setError(null);
                paytmPhone.setError(null);
                try {
                    addmoney = Integer.parseInt(mywithdrawAmount);
                    newmyINR = Integer.parseInt(walletAmount);
                } catch (NumberFormatException ex) { // handle your exception

                }

                if (TextUtils.isEmpty(mywithdrawAmount)) {
                    withdrawmoney.setError("This field is required");
                    cancel = true;
                }
                 else if (TextUtils.isEmpty(mypaytmPhone)) {
                    paytmPhone.setError("This field is required");
                    cancel = true;
                }
                else if(!mypaytmPhone.matches(regexStr)) {
                    paytmPhone.setError("Please Enter Valid Phone no.");
                    cancel = true;
                }
                 else if ((addmoney <= 0)) {
                    withdrawmoney.setError("Please enter amount more than 0");
                    cancel = true;
                } else if ((addmoney > newmyINR)) {
                    withdrawmoney.setError("You do not have sufficient amount to withdraw");
                    cancel = true;
                } else {

                    withdrawMoney();


                }
            }
        });

        mSwipeRefreshLayout = findViewById(R.id.swipeToRefreshWithdraw);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getWithrawData();
                arrmodelwithdrawhistory.clear();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });


        getWithrawData();


    }

    private void getWithrawData() {
        networkCall.NetworkAPICall(ApiURL.withdraw_history, true);
    }

    private void withdrawMoney() {
        networkCall.NetworkAPICall(ApiURL.withdraw_request, true);
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.withdraw_request:
                ion = (Builders.Any.B) Ion.with(withdrawal.this)
                        .load("POST", ApiURL.withdraw_request)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("paytm_number", mypaytmPhone)
                        .setBodyParameter("amount", mywithdrawAmount);
                break;

            case ApiURL.withdraw_history:
                ion = (Builders.Any.B) Ion.with(withdrawal.this)
                        .load("POST", ApiURL.withdraw_history)
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.withdraw_request:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");

//                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);

                        walletAmount = jsonObject1.getString("tolat_amount");
                        status = jsonObject1.getString("withdrawal_request");

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();

                        editor.putString("walllet_amount", walletAmount);
                        editor.putString("withdrawal_request", status);
                        editor.commit();


                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Loot 2021");
                        sweetAlertDialog.setContentText("Successful");
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(withdrawal.this, R.color.black));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                getWithrawData();
                                arrmodelwithdrawhistory.clear();
                                sweetAlertDialog.dismiss();
                                withdrawmoney.clearFocus();
                                withdrawmoney.setText("");
                                paytmPhone.clearFocus();
                                paytmPhone.setText("");
                                if (walletAmount.equals("null")) {
                                    amount.setText("0");
                                } else {
                                    amount.setText(walletAmount);
                                }

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();

                    } else {

                        Toast.makeText(withdrawal.this, "Check every field and try again...".toString(), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e1) {
                    Toast.makeText(withdrawal.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.withdraw_history:
                try {
                    JSONObject withdrawHistory = new JSONObject(jsonstring.toString());
                    String status = withdrawHistory.getString("success");
                    if (status.equals("true")) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        JSONArray withdrawHistoryList = withdrawHistory.getJSONArray("data");
                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_withdraw_history = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_wthdraw_list.class);
                            arrmodelwithdrawhistory.add(model_withdraw_history);

                        }

                        withdrawalHistory_recy_adapter = new withdraw_list_recy_adapter(arrmodelwithdrawhistory, withdrawal.this, R.layout.modellayout, this, 1);

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);

                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setHasFixedSize(true);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);
                        recyclerView.setAdapter(withdrawalHistory_recy_adapter);

                    } else {

                        String status_fail = withdrawHistory.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(withdrawal.this, "Data not found", Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }
                    }
                }

                catch (JSONException e1) {
                    Toast.makeText(withdrawal.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {


        Model_wthdraw_list model_withdraw_history = arrmodelwithdrawhistory.get(position);


        TextView date = view.findViewById(R.id.date);
        TextView withdrawal_status = view.findViewById(R.id.witdraw_status);
        TextView withdrawAmount = view.findViewById(R.id.Withdrawamount);
        model_withdraw_history.getCreated_at();
        model_withdraw_history.getWalllet_amount();
        String mdate = "";
        mdate = model_withdraw_history.getCreated_at();
        date.setText(mdate);
        String mwithdrawAmount = "";
        mwithdrawAmount = model_withdraw_history.getWalllet_amount();
        withdrawAmount.setText("₹" + " " + mwithdrawAmount);


        if (model_withdraw_history.getWithdrawal_request().equals("Pending")) {
            status = model_withdraw_history.getWithdrawal_request();
            withdrawal_status.setText(status);
            withdrawal_status.setTextColor(Color.parseColor("#FFA500"));
        } else if (model_withdraw_history.getWithdrawal_request().equals("Active")) {
            status = model_withdraw_history.getWithdrawal_request();

            withdrawal_status.setText(status);
            withdrawal_status.setTextColor(Color.parseColor("#008000"));

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(withdrawal.this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}