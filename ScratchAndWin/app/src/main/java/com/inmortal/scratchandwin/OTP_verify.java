package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class OTP_verify extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    EditText otpEdt;
    String myotpEdt;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    Progress progress;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    TextView num;
    String message;
    Boolean cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verify);

        progress = new Progress(OTP_verify.this);
        networkCall = new NetworkCall(OTP_verify.this, OTP_verify.this);
        otpEdt = (EditText) findViewById(R.id.otpEditText);
        Intent intentnum = getIntent();
         message = intentnum.getStringExtra("key");
        num = findViewById(R.id.setNumber);
        num.setText(message);

    }

    @Override
    public void onBackPressed() {
        Intent inte = new Intent(OTP_verify.this,ForgotPassword.class);
        startActivity(inte);
        OTP_verify.this.finish();
    }

    public void back(View view) {
        Intent inten = new Intent(OTP_verify.this,ForgotPassword.class);
        startActivity(inten);
        OTP_verify.this.finish();
    }

    public void change(View view) {
        myotpEdt = otpEdt.getText().toString().trim();
        otpEdt.setError(null);
        if (TextUtils.isEmpty(myotpEdt)) {
            otpEdt.setError("OTP is required");
            cancel = true;
        }
        else if(myotpEdt.length()<4) {
            otpEdt.setError("Please Enter Valid OTP");
            cancel = true;
        }else{

            otpverify();
        }

    }

    private void otpverify() {
        networkCall.NetworkAPICall(ApiURL.otp_verify, true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.otp_verify:
                ion = (Builders.Any.B) Ion.with(OTP_verify.this)
                        .load("POST", ApiURL.otp_verify)
                        .setBodyParameter("otp", myotpEdt)
                        .setBodyParameter("number", message);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.otp_verify:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {


                        Toast.makeText(OTP_verify.this, msg, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(OTP_verify.this, change_pass.class);
                        intent.putExtra("key", message);
                        startActivity(intent);
                        finish();
                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Loot 2021")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Loot 2021")
                            .setContentText("Oops... Something went wrong!")
                            .show();
                    // Toast.makeText(login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}