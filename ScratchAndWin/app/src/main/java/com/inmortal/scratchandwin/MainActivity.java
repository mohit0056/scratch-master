package com.inmortal.scratchandwin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.navigation.NavigationView;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.codeshuffle.scratchcardlayout.listener.ScratchListener;
import in.codeshuffle.scratchcardlayout.ui.ScratchCardLayout;
import in.codeshuffle.scratchcardlayout.util.ScratchCardUtils;

public class MainActivity extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter.ReturnView, ScratchListener, NavigationView.OnNavigationItemSelectedListener {
    static final String pref_name = "Scratch";
    RecyclerView recyclerView;
    Adapter adapter;
    ArrayList<ScratchModel> arrScratchModel = new ArrayList<>();
    Progress progress;
    ScratchModel scratchModel;
    ScratchCardLayout scratchCardLayout;
    boolean doubleBackToExitPressedOnce = false;
    String scratchAmount, list_Scratch_id;
    SweetAlertDialog sweetAlertDialog;

    TextView amountt;
    int i;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private DrawerLayout mDrawer;
    InterstitialAd interstitialAd = null;
    int Position;
    Toolbar mToolbar;
    private AdView bannerAdView;
    RelativeLayout txtDialogg;
    SharedPreferences mSharedPreference;
    String myname = "", mymobile_number = "", userid = "", walletAmount = "0", id, Scrathedamount, Amountwallet;
    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = new Progress(MainActivity.this);
        networkCall = new NetworkCall(MainActivity.this, MainActivity.this);

//        interstitialAd= new InterstitialAd(this);
//        interstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen_test));
//        AdRequest adRequest = new AdRequest.Builder().build();
//        interstitialAd.loadAd(adRequest);

        AdRequest adRequest1 = new AdRequest.Builder().build();

        // load Ad with the Request
        bannerAdView = findViewById(R.id.bannerAdView);
        bannerAdView.loadAd(adRequest1);
        bannerAdView.setVisibility(View.VISIBLE);


        mDrawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.vNavigation);
        navigationView.setNavigationItemSelectedListener(this);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);

        mToolbar = (Toolbar) findViewById(R.id.main_appbar);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mymobile_number = (mSharedPreference.getString("mobile", ""));
        myname = (mSharedPreference.getString("name", ""));
        scratchAmount = (mSharedPreference.getString("scratchAmount", ""));
        userid = (mSharedPreference.getString("id", ""));
        Amountwallet = (mSharedPreference.getString("walllet_amount", ""));


        amountt = findViewById(R.id.amountt);

        txtDialogg = findViewById(R.id.comingsoon);


        if (Amountwallet.equals("null")) {
            amountt.setText("0");
        } else {
            amountt.setText(Amountwallet);
        }


        recyclerView = (RecyclerView) findViewById(R.id.recycler_Scratch);

        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Loaddata();
                arrScratchModel.clear();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        Loaddata();

        setupToolbar();
    }

    private void Loaddata() {
        networkCall.NetworkAPICall(ApiURL.Scratch_list, true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Scratch_list:
                ion = (Builders.Any.B) Ion.with(getApplicationContext())
                        .load("POST", ApiURL.Scratch_list)
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.Scratch_list:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    if (succes.equals("true")) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");
                        if (jsonObject1.length() == 0) {
                            txtDialogg.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < jsonObject1.length(); i++) {
                                scratchModel = new Gson().fromJson(jsonObject1.optJSONObject(i).toString(), ScratchModel.class);
                                arrScratchModel.add(scratchModel);
                            }
                            adapter = new Adapter(arrScratchModel, MainActivity.this, R.layout.alertdialog, this, 1);
                            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setAdapter(adapter);
                        }
                    } else {
                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(getApplicationContext(), jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(MainActivity.this, jsonstring, Toast.LENGTH_SHORT).show();
    }

    public void profile(View view) {
        Intent intent = new Intent(MainActivity.this, withdrawal.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        ScratchModel scratchModel = arrScratchModel.get(position);


        RelativeLayout relativeLayout_D;
        ScratchCardLayout scratched;
        CardView cd;
        TextView Alreadyscratched;

        relativeLayout_D = view.findViewById(R.id.relative_Dialog); //initialisTION
        Animation slideUp = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slidedown);

        scratched = view.findViewById(R.id.scratchCard); //initialisTION
        cd = view.findViewById(R.id.cdsc);
        Alreadyscratched = view.findViewById(R.id.AlreadyScratched);


        relativeLayout_D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast t = Toast.makeText(MainActivity.this,"msg"+scratchModel, Toast.LENGTH_LONG);
//                t.show();


                Dialog dialogg = new Dialog(MainActivity.this, R.style.PauseDialog);
                dialogg.setContentView(R.layout.alert);
                TextView price = dialogg.findViewById(R.id.priceTxt);
                price.setText(arrScratchModel.get(position).getSkretch_amount());
                id = String.valueOf(arrScratchModel.get(position).getId());
                String st = arrScratchModel.get(position).getSkretch_amount();


                if (st.equals("0")) {
                    scratchAmount = "Better luck next time";

                    price.setText(scratchAmount);

                }

                else {

//                    String st1 = st.replaceAll("[^A-Za-z]", "");
//                    String st2 = st.replaceAll("[^0-9]", "");
//                    System.out.println("String b = " + st1);
//                    scratchAmount = st2;

                    price.setText("You have won ₹" + st);

                    interstitialAd = new InterstitialAd(MainActivity.this);
                    interstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
                    AdRequest adRequest = new AdRequest.Builder().build();
                    interstitialAd.loadAd(adRequest);
                    interstitialAd.setAdListener(new AdListener() {
                        public void onAdLoaded() {

                            if (interstitialAd.isLoaded()) {
                                interstitialAd.show();
                            }
                        }
                    });


                }


                Window window = dialogg.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.CENTER;

                scratchCardLayout = dialogg.findViewById(R.id.scratchCard);
                scratchCardLayout.setScratchListener(MainActivity.this);

                scratchCardLayout.setScratchWidthDip(ScratchCardUtils.dipToPx(getApplicationContext(), 40));
                scratchCardLayout.setScratchDrawable(getResources().getDrawable(R.drawable.scratch));
                scratchCardLayout.setRevealFullAtPercent(40);
                scratchCardLayout.setScratchEnabled(true);


//              interstitialAd.show();


                ImageView imageView = dialogg.findViewById(R.id.cancelBtn);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        list_Scratch_id = String.valueOf(scratchModel.getId());

//                        scratchAmount=scratchModel.getSkretch_amount();

                        String st = arrScratchModel.get(position).getSkretch_amount();

                        if (st.equals("0")) {
                            scratchAmount = "0";
                        } else {

                            String st1 = st.replaceAll("[^A-Za-z]", "");
                            String st2 = st.replaceAll("[^0-9]", "");
                            System.out.println("String b = " + st1);

                            scratchAmount = st2;
                        }

                        wallet();
                        cd.setVisibility(View.VISIBLE);
                        relativeLayout_D.setVisibility(View.INVISIBLE);

                        if (arrScratchModel.get(position).getSkretch_amount().equals("0")) {
                            Alreadyscratched.setText("Better Luck Next Time");
                        } else {

                            Alreadyscratched.setText("You have won ₹" + arrScratchModel.get(position).getSkretch_amount());

                        }
                        dialogg.dismiss();

                        // Toast.makeText(MainActivity.this, "Dismissed..!!", Toast.LENGTH_SHORT).show();
                    }
                });

                dialogg.show();
                dialogg.setCanceledOnTouchOutside(false);

            }


        });


    }

    private void wallet() {


        Ion.with(MainActivity.this)
                .load("POST", ApiURL.wallet)
                .setBodyParameter("user_id", userid)
                .setBodyParameter("skretch_amount", scratchAmount)
                .setBodyParameter("skretch_id", list_Scratch_id)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {

                            JSONObject jsonObject = new JSONObject(result);
                            String status = jsonObject.getString("success");
                            if (status.equals("true")) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                walletAmount = jsonObject1.getString("wallet_amount");
                                amountt.setText(walletAmount);
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("id", userid);
                                editor.putString("walllet_amount", walletAmount);
                                // editor.putString("token", token);
                                editor.commit();
                                Toast.makeText(MainActivity.this, "Wallet Updated", Toast.LENGTH_SHORT).show();
                            } else {
                                String fail_status = jsonObject.getString("success");
                                //   String msg = jsonObject.getString("message");

                                if (fail_status.equals("false")) {
                                    //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e1) {
                            Toast.makeText(MainActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onScratchStarted() {

    }

    @Override
    public void onScratchProgress(ScratchCardLayout scratchCardLayout, int atLeastScratchedPercent) {

    }

    @Override
    public void onScratchComplete() {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share) {
            Intent i = new Intent(

                    Intent.ACTION_SEND);

            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Scratch And Win");
            i.putExtra(

                    Intent.EXTRA_TEXT, "Take a look at this awesome app, I earned a massive amount of money.\n" +
                            "You can also earn a massive amount of money without any investment.  \n" +
                            "Click Below to download this Awesome app\n" +
                            "https://play.google.com/store/apps/details?id=com.inmortal.scratchandwin");

            startActivity(Intent.createChooser(

                    i,

                    "Share this to your friends..."));

        } else if (id == R.id.about) {

            //    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://inmortaltechnologies.com/about.html")));

            Intent about = new Intent(MainActivity.this, About.class);
            startActivity(about);
        } else if (id == R.id.rate) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.inmortal.scratchandwin")));
        } else if (id == R.id.refresh) {

            Loaddata();
            arrScratchModel.clear();
        } else if (id == R.id.logout) {


            sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setTitleText("Loot 2021");
            sweetAlertDialog.setContentText("Are you sure?");
            sweetAlertDialog.show();

            Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
            btn.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.black));

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {


                    Intent intent = new Intent(MainActivity.this, login.class);
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                    editor.clear();
                    editor.apply();
                    editor.remove("login");//your key
                    startActivity(intent);
                    MainActivity.this.finish();

                }
            });
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();


            return true;

        } else if (id == R.id.contactUs) {
            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://inmortaltechnologies.com/Contact.html")));
            Intent contact = new Intent(MainActivity.this, contact.class);
            startActivity(contact);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    protected void setupToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.nav);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(Gravity.LEFT);
            }
        });
    }

    @Override
    public void onBackPressed() {
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitleText("Loot 2021");
        sweetAlertDialog.setContentText("Are you sure?");
        sweetAlertDialog.show();

        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.black));


        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                MainActivity.this.finish();

            }
        });
    }

}