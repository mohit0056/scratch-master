package com.inmortal.scratchandwin.utill;

public class ApiURL {
    public final static String main_url = "http://inmortaltech.com/Skretch-APIs/public/api/";
    public final static String Scratch_list = main_url+"Skretch_list";
    public final static String user_login = main_url+"login_skretch";
    public final static String user_register = main_url+"register_skretch";
    public final static String wallet = main_url+"add_skretch";
    public final static String withdraw_request = main_url+"user_wallet_amount";
    public final static String withdraw_history = main_url+"show_user_amount";
    public final static String change_password = main_url+"change_password";
    public final static String otp_verify = main_url+"otp_verified";
    public final static String number_otp = main_url+"sentotp";

    public final static String Login_Token = "b27585828a675f5acfef052dd1a8cf0c6c1ee4b0";
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
}
