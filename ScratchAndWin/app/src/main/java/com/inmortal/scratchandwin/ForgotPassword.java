package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class ForgotPassword extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    EditText forgotphone_NO;
    String myforgotphone_NO;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    Progress progress;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    Boolean cancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password2);

        progress = new Progress(ForgotPassword.this);
        networkCall = new NetworkCall(ForgotPassword.this, ForgotPassword.this);

        forgotphone_NO = (EditText) findViewById(R.id.forgotphno);
    }

    public void back(View view) {
        Intent inte = new Intent(ForgotPassword.this,login.class);
        startActivity(inte);
        ForgotPassword.this.finish();
    }

    public void sendOTP(View view) {

        myforgotphone_NO = forgotphone_NO.getText().toString().trim();

        forgotphone_NO.setError(null);

        if (TextUtils.isEmpty(myforgotphone_NO)) {
            forgotphone_NO.setError("Phone number is Required");
            cancel = true;
        }
        else if(!myforgotphone_NO.matches(regexStr)) {
            forgotphone_NO.setError("Please Enter Valid Phone no.");
            cancel = true;
        }else{

            sendotp();
        }

    }

    private void sendotp() {
        networkCall.NetworkAPICall(ApiURL.number_otp, true);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ForgotPassword.this,login.class);
        startActivity(intent);
        ForgotPassword.this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.number_otp:
                ion = (Builders.Any.B) Ion.with(ForgotPassword.this)
                        .load("POST", ApiURL.number_otp)
                        .setBodyParameter("number", myforgotphone_NO);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.number_otp:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {


                        Intent inten = new Intent(ForgotPassword.this,OTP_verify.class);

                        inten.putExtra("key", myforgotphone_NO);

                        startActivity(inten);

                        ForgotPassword.this.finish();
                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Loot 2021")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Loot 2021")
                            .setContentText("Oops... Something went wrong!")
                            .show();
                    // Toast.makeText(login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}