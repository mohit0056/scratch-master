package com.inmortal.scratchandwin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ScratchHolder> {
    OnLoadInterface onLoadInterface;
    private List list;
    private Context context;
    private int layout;
    ReturnView returnView;
    int from;

    public interface ReturnView {
        void getAdapterView(View view, List objects, int position, int from);
    }

    public Adapter(List list1, Context context, int layout, ReturnView returnView, int from) {
        this.list = list1;
        this.context = context;
        this.layout = layout;
        this.returnView = returnView;
        this.from = from;

    }

    @NonNull
    @Override
    public Adapter.ScratchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.alertdialog, parent, false);
        return new Adapter.ScratchHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ScratchHolder holder, int position) {
        returnView.getAdapterView(holder.itemView, list, position, from);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ScratchHolder extends RecyclerView.ViewHolder {
        public ScratchHolder(@NonNull View itemView) {
            super(itemView);

        }
    }
}
