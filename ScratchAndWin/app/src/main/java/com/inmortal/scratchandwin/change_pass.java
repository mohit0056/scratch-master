package com.inmortal.scratchandwin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.inmortal.scratchandwin.utill.ApiURL;
import com.inmortal.scratchandwin.utill.NetworkCall;
import com.inmortal.scratchandwin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class change_pass extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    EditText chngPass;
    String mychngPass,number;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    Progress progress;
    SharedPreferences mSharedPreference;
    TextView phnum;
    SweetAlertDialog sweetAlertDialog;

    NetworkCall networkCall;
    Boolean cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        progress = new Progress(change_pass.this);
        networkCall = new NetworkCall(change_pass.this, change_pass.this);
        chngPass = (EditText) findViewById(R.id.newPassEdt);
        phnum = (TextView) findViewById(R.id.phnum);
        Intent intentnum = getIntent();
        number = intentnum.getStringExtra("key");
        phnum.setText(number);

    }

    public void back(View view) {
        Intent inten = new Intent(change_pass.this,OTP_verify.class);
        startActivity(inten);
        change_pass.this.finish();
    }

    @Override
    public void onBackPressed() {
        Intent inte = new Intent(change_pass.this,OTP_verify.class);
        startActivity(inte);
        change_pass.this.finish();
    }

    public void login(View view) {
        mychngPass = chngPass.getText().toString().trim();

        chngPass.setError(null);


          if (TextUtils.isEmpty(mychngPass)) {
              chngPass.setError("Password is required");
            cancel = true;
        }

        else if (chngPass.length() < 6) {
              chngPass.setError("Password minimum contain 6 character");
            cancel = true;
        }


        else{

            changePass();
        }




    }

    private void changePass() {
        networkCall.NetworkAPICall(ApiURL.change_password, true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.change_password:
                ion = (Builders.Any.B) Ion.with(change_pass.this)
                        .load("POST", ApiURL.change_password)
                        .setBodyParameter("number", number)
                        .setBodyParameter("new_password", mychngPass);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.change_password:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {

                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Loot 2021");
                        sweetAlertDialog.setContentText("Password changed successfully");
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(change_pass.this, R.color.black));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                Intent intent = new Intent(change_pass.this, login.class);
                                startActivity(intent);
                                finish();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();




                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Loot 2021")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Loot 2021")
                            .setContentText("Oops... Something went wrong!")
                            .show();
                    // Toast.makeText(login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(change_pass.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}